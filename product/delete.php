<?php
if(empty($_POST['product_id'])){
    header('Location:index.php');
}

require_once 'config/db.php';
require_once 'classes/Product.php';

Product::delete($_POST['product_id'], $pdo);

header('Location:index.php');