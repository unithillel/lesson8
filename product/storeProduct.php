<?php
require_once '../config/db.php';
require_once '../classes/Product.php';


if (!empty($_POST['title'])) {
    $product = new Product($_POST['title'], $_POST['price'], $_POST['description'], $_POST['category_id']);
    $product->store($pdo);
    header('Location:/product/index.php');
}