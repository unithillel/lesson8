<?php
require_once 'config/db.php';


try {
    $categoriesSql = "CREATE TABLE categories (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";
    $pdo->exec($categoriesSql);

    $sql = "CREATE TABLE products (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    price FLOAT,
    description TEXT,
    category_id INT,
    date_created DATE NOT NULL,
    FOREIGN KEY (category_id) REFERENCES categories (id)
    ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";
    $pdo->exec($sql);



    // 1 -> 1,2 ,3 ,4
    // 2 - 5


} catch (Exception $exception) {
    echo "Error creating table! " . $exception->getCode() . ' message: ' . $exception->getMessage();
    die();
}

header('Location:index.php');