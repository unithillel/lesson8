<?php
require_once 'config/db.php';
require_once 'classes/Product.php';

if (!empty($_POST['title'])) {

    Product::upgrade($_POST['product_id'], $_POST['title'], $_POST['price'], $_POST['description'], $pdo);

    header('Location:index.php');
}