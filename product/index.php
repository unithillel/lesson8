<?php
require_once '../config/db.php';
require_once '../classes/DBEntity.php';
require_once '../classes/Category.php';
require_once '../classes/Product.php';
require_once '../classes/Cart.php';
$products = Product::all($pdo);
$cart = null;

if (!empty($_COOKIE['cart'])) {
    $cart = new Cart();
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div>
        <ul>
            <li>
                <a href="createProductTable.php">Create Table</a>
            </li>
            <li>
                <a href="addProduct.php">Add new product</a>
            </li>
        </ul>
    </div>
    <div>
        <table>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Price</th>
                <th></th>
                <th>Category</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>

            <?php foreach ($products as $product):?>
                <tr>
                    <td><?=$product->getId()?></td>
                    <td><?=$product->getTitle()?></td>
                    <td><?=$product->getPrice()?></td>
                    <td><a href="/cart/add.php?product_id=<?=$product->getId()?>">Buy</a></td>
                    <td><a href="/category/show.php?id=<?=$product->getCategory()->getId()?>"><?=$product->getCategory()->getTitle()?></a></td>
                    <td><a href="details.php?id=<?=$product->getId()?>">Details</a></td>
                    <td><a href="editProduct.php?id=<?=$product->getId()?>">Edit</a></td>
                    <td>
                        <form action="delete.php" method="post">
                            <input type="hidden" name="product_id" value="<?=$product->getId()?>">
                            <button>Delete</button>
                        </form>
                    </td>
                </tr>
            <?php endforeach;?>

        </table>
    </div>
    <?php if($cart):?>
        <div>
            <h2>Cart</h2>
            <table>
                <tr>
                    <th>ID</th>
                    <th>Product name</th>
                    <th>Amount</th>
                </tr>
                <?php foreach ($cart->getCart() as $cartElement):?>
                    <tr>
                        <td><?=$cartElement['product']->getId()?></td>
                        <td><?=$cartElement['product']->getTitle()?></td>
                        <td>
                            <a href="/cart/add.php?product_id=<?=$cartElement['product']->getId()?>">+</a>
                            <?=$cartElement['amount']?>
                            <a href="/cart/remove.php?product_id=<?=$cartElement['product']->getId()?>">-</a>
                        </td>
                    </tr>
                <?php endforeach;?>

            </table>
            <p>
                <strong>Total price: <?=$cart->getTotalPrice()?></strong>
            </p>
            <p>
                <a href="/order/create.php">Create order</a>
            </p>
        </div>
    <?php endif;?>
</body>
</html>
