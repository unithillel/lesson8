<?php
if(empty($_GET['id'])){
   header('Location:index.php');
}
require_once '../config/db.php';
require_once '../classes/Product.php';
$product = Product::getById($_GET['id'], $pdo);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1><?=$product->getTitle()?></h1>

    <ul>
        <li>Price: <?=$product->getPrice()?></li>
        <li><?=$product->getDescription()?></li>
        <li>Added: <?=$product->getCreatedAt()?></li>
    </ul>
</body>
</html>


