<?php
require_once '../config/db.php';
require_once '../classes/Category.php';
$categories = Category::all($pdo);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Add new product</h1>
    <form action="storeProduct.php" method="post">
        <div>
            <label>Title: <input type="text" name="title"></label>
        </div>
        <div>
            <label>Price: <input type="text" name="price"></label>
        </div>
        <div>
            <label>Description: <textarea name="description"></textarea></label>
        </div>
        <div>
            <label for="category">Select category:</label>
            <select name="category_id" id="category">
                <?php foreach($categories as $category):?>
                    <option value="<?=$category->getId()?>"><?=$category->getTitle()?></option>
                <?php endforeach;?>
            </select>
        </div>
        <button>Save</button>
    </form>
</html>