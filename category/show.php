<?php
if(empty($_GET['id'])){
    header('Location:404.php');
    die();
}
require_once '../config/db.php';
require_once '../classes/Category.php';
require_once '../classes/Product.php';

$id = htmlspecialchars($_GET['id']);
$category = Category::getById($id, $pdo);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$category->getTitle()?></title>
</head>
<body>
<h1>Products category - <?=$category->getTitle()?>:</h1>
<ul>
    <?php foreach ($category->getProducts() as $product):?>
        <li><a href="/product/details.php?id=<?=$product->getId();?>"><?=$product->getTitle();?></a></li>
    <?php endforeach;?>
</ul>

</body>
</html>



