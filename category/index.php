<?php
require_once '../config/db.php';
require_once '../classes/Product.php';
require_once '../classes/Category.php';

$categories = Category::all($pdo);
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Categories:</h1>
    <?php foreach ($categories as $category):?>
        <div>
            <h2><?=$category->getTitle()?></h2>
            <ul>
                <?php foreach ($category->getProducts($pdo) as $product):?>
                    <li><a href="/product/details.php?id=<?=$product->getId()?>"><?=$product->getTitle()?></a></li>
                <?php endforeach;?>
            </ul>
        </div>
    <?php endforeach ;?>

</body>
</html>
