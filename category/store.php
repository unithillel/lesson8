<?php
require_once '../config/db.php';
require_once '../classes/Category.php';

if(!empty($_POST['title'])){
    $title = htmlspecialchars($_POST['title']);
    $category = new Category($title);
    $category->store($pdo);
}
header('Location:/category/index.php');