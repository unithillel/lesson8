<?php
require_once '../classes/Cart.php';
if(!empty($_REQUEST['product_id'])){
    $cart = new Cart();
    $cart->removeProduct($_REQUEST['product_id']);
}
header('Location: ' . $_SERVER['HTTP_REFERER']);