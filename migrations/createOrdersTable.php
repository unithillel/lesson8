<?php
require_once '../config/db.php';

$ordersSQL = "
    CREATE TABLE orders(
        id int PRIMARY KEY AUTO_INCREMENT NOT NULL,
        user_name VARCHAR(255) NOT NULL,
        phone VARCHAR(255) NOT NULL
    )DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;
";
$pdo->exec($ordersSQL);

$orderProductSQL = "
    CREATE TABLE order_product(
        order_id INT,
        product_id INT,
        amount INT,
        FOREIGN KEY (order_id) REFERENCES orders (id) ON DELETE CASCADE,
        FOREIGN KEY (product_id) REFERENCES products (id) ON DELETE CASCADE
    )DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;
";

$pdo->exec($orderProductSQL);