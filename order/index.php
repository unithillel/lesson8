<?php
require_once '../config/db.php';
require_once '../classes/Order.php';
$orders = Order::all($pdo);
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Orders:</title>
</head>
<body>
    <table>
        <tr>
            <th>id</th>
            <th>Client</th>
            <th>Phone</th>
            <th>Products</th>
        </tr>

        <?php foreach($orders as $order):?>
            <tr>
                <td><?=$order->getId()?></td>
                <td><?=$order->getUserName()?></td>
                <td><?=$order->getPhone()?></td>
                <td>
                    <ul>
                        <?php foreach ($order->products() as $product):?>
                            <li> <?=$product['amount']?> x <strong><?=$product['obj']->getTitle()?></strong> <?=$product['obj']->getPrice()?></li>
                        <?php endforeach;?>
                    </ul>
                </td>
            </tr>
        <?php endforeach;?>
    </table>
</body>
</html>
