<?php
require_once '../classes/Cart.php';
$cart = null;
if (!empty($_COOKIE['cart'])) {
    $cart = new Cart();
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create new order</title>
</head>
<body>
    <h1>Your order</h1>
    <?php if($cart):?>
        <div>
            <h2>Cart</h2>
            <table>
                <tr>
                    <th>ID</th>
                    <th>Product name</th>
                    <th>Amount</th>
                </tr>
                <?php foreach ($cart->getCart() as $cartElement):?>
                    <tr>
                        <td><?=$cartElement['product']->getId()?></td>
                        <td><?=$cartElement['product']->getTitle()?></td>
                        <td>
                            <a href="/cart/add.php?product_id=<?=$cartElement['product']->getId()?>">+</a>
                            <?=$cartElement['amount']?>
                            <a href="/cart/remove.php?product_id=<?=$cartElement['product']->getId()?>">-</a>
                        </td>
                    </tr>
                <?php endforeach;?>

            </table>
            <p>
                <strong>Total price: <?=$cart->getTotalPrice()?></strong>
            </p>
        </div>
    <?php endif;?>
    <div>
        <form action="/order/store.php" method="POST">
            <label>
                Your name:
                <input type="text" name="user_name">
            </label>
            <br>
            <label>
                Your phone:
                <input type="text" name="phone">
            </label>
            <br>
            <button>Create order</button>
        </form>
    </div>


</body>
</html>
