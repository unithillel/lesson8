<?php
require_once '../classes/Order.php';

if (!empty($_POST['user_name'])) {
    $order = new Order($_POST['user_name'], $_POST['phone']);
    $order->store();
    header('Location:/product/');
}