<?php
require_once 'DBEntity.php';
require_once 'Category.php';

class Product extends DBEntity
{
    // $this->db
    protected $id;
    protected $title;
    protected $price;
    protected $description;
    protected $createdAt;
    protected $categoryId;

    public function __construct($title, $price, $description, $categoryId = NULL)
    {
        parent::__construct();
        $this->title = htmlspecialchars($title);
        $this->price = htmlspecialchars($price);
        $this->description = htmlspecialchars($description);
        $this->categoryId = $categoryId;
    }

    /**
     * @return Category|null
     */
    public function getCategory()
    {
        $statement = $this->db->prepare('SELECT * FROM categories WHERE id=:id');
        $statement->bindValue(':id', $this->categoryId);
        $statement->execute();
        $categoryArr = $statement->fetchAll();
        $category = new Category($categoryArr[0]['title']);
        $category->setId($categoryArr[0]['id']);
        return $category;
    }

    static public function getById($id, PDO $pdo){
        $id = htmlspecialchars($id);
        try {
            $sql = 'SELECT * FROM products WHERE id=:id';
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':id', $id);
            $statement->execute();
            $productsArr = $statement->fetchAll();
            $productArr = $productsArr[0];
            $productObj = new self($productArr['title'], $productArr['price'], $productArr['description']);
            $productObj->setId($productArr['id']);
            $productObj->setCreatedAt($productArr['date_created']);
            return $productObj;
        }catch (Exception $exception){
            header('Location:404.php');
//    echo "Error getting products! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    static public function delete($id, PDO $pdo){
       $product = self::getById($id, $pdo);
       $product->destroy($pdo);
    }

    static public function upgrade($id, $title, $price, $description, Pdo $pdo){
        $product = new Product($title, $price, $description);
        $product->setId($id);
        $product->update($pdo);
    }
    
    static public function all($pdo){
        try {
            $sql = 'SELECT * FROM products';
            $pdoResult = $pdo->query($sql);
            $productsArr = $pdoResult->fetchAll();
            $productObjs = [];
            foreach ($productsArr as $productArr){
                $productObj = new self($productArr['title'], $productArr['price'],
                    $productArr['description'], $productArr['category_id']);
                $productObj->setId($productArr['id']);
                $productObj->setCreatedAt($productArr['date_created']);
                $productObjs[] = $productObj;
            }

            return $productObjs;

        }catch (Exception $exception){
            echo "Error getting products! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    public function store(PDO $pdo){
        try {
            $sql = 'INSERT INTO products SET 
            title = :title_param,
            price = :price_param,
            description = :description_param,
            category_id = :category_id,
            date_created = CURDATE();
        ';
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':title_param', $this->title);
            $statement->bindValue(':price_param', $this->price);
            $statement->bindValue(':description_param', $this->description);
            $statement->bindValue(':category_id', $this->categoryId);
            $statement->execute();

        }catch (Exception $exception){
            echo "Error storing product! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    public function update(PDO $pdo){
        try {
            $sql = 'UPDATE products SET
            title = :title,
            price = :price,
            description = :description
            WHERE id = :id';


            $statement = $pdo->prepare($sql);
            $statement->execute([
                ':title' => $this->title,
                ':price' => $this->price,
                ':description' => $this->description,
                ':id' => $this->id,
            ]);

        }catch (Exception $exception){
            echo "Error updating product! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    public function destroy($pdo){
        try {
            $sql = "DELETE FROM products WHERE id=:id";
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':id', $this->id);
            $statement->execute();
        }catch (Exception $exception){
            echo "Error deleting product! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die();
        }
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    
}