<?php
require_once 'DBEntity.php';
require_once 'Product.php';

class Cart extends DBEntity
{
    protected $products = [];
    const CARTTTL = 60 * 60 * 24 * 30;

    public function __construct()
    {
        parent::__construct();
        if (!empty($_COOKIE['cart'])) {
            $this->products = json_decode($_COOKIE['cart'], true);
        }
    }

    public function addProduct($id)
    {
        if (key_exists($id, $this->products)) {
            $this->products[$id]++;
        } else {
            $this->products[$id] = 1;
        }
        setcookie('cart', json_encode($this->products), time() + self::CARTTTL, '/');
    }
    public function removeProduct($id)
    {
        if (key_exists($id, $this->products)) {
            $this->products[$id]--;
            if($this->products[$id] <= 0){
                unset($this->products[$id]);
            }
            setcookie('cart', json_encode($this->products), time() + self::CARTTTL, '/');
        }

    }

    public function getProducts()
    {
        return $this->products;
    }

    public function getCart(){
        $cart = [];
        foreach ($this->products as $productId => $productAmount){
            $cart[] = [
                'amount' => $productAmount,
                'product' => Product::getById($productId, $this->db)
            ];
        }
        return $cart;
    }
    public function getTotalPrice(){
        $totalPrice = 0;
        foreach ($this->products as $productId => $productAmount){
            $product = Product::getById($productId, $this->db);
            $totalPrice += $product->getPrice() * $productAmount;
        }
        return round($totalPrice, 2);
    }

    public function clear(){
        setcookie('cart', '', time()-1, '/');
    }

}