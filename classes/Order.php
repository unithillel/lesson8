<?php
require_once 'Cart.php';
require_once 'DBEntity.php';
require_once 'Product.php';

class Order extends DBEntity
{
    protected $id = 0;
    protected $userName = '';
    protected $phone = '';


    public function __construct($userName, $phone)
    {
        parent::__construct();
        $this->userName = $userName;
        $this->phone = $phone;
    }

    public function store(){
        try {
            $statement = $this->db->prepare("INSERT INTO orders SET 
                user_name = :user_name,
                phone = :phone
             ");
            $statement->bindValue(':user_name', $this->userName);
            $statement->bindValue(':phone', $this->phone);
            $statement->execute();
            $this->id = $this->db->lastInsertId();

        }catch (Exception $exception){
            die('Error while saving order! '.$exception->getMessage());
        }

        $cart = new Cart();
        foreach ($cart->getProducts() as $productKey => $amount){
            $this->addProduct($productKey, $amount);
        }
        $cart->clear();

        return true;
    }

    public function addProduct($productId, $amount){
        try{
            $orderProductStatement = $this->db->prepare("INSERT INTO order_product SET
                order_id = :order_id,
                product_id = :product_id,
                amount = :amount
            ");
            $orderProductStatement->bindValue(':order_id', $this->id);
            $orderProductStatement->bindValue(':product_id', $productId);
            $orderProductStatement->bindValue(':amount', $amount);
            $orderProductStatement->execute();
        }catch (Exception $e){
            die('Error while saving order_product! '.$e->getMessage());
        }
    }

    static public function all($pdo){
        $sql = "SELECT * from orders";
        $statement = $pdo->query($sql);
        $ordersArr = $statement->fetchAll();
        $orderObjs = [];
        foreach ($ordersArr as $orderArr){
            $orderObj = new self($orderArr['user_name'], $orderArr['phone']);
            $orderObj->setId($orderArr['id']);
            $orderObjs[] = $orderObj;
        }
        return $orderObjs;
    }

    public function products(){
        $sql = "SELECT p.id, op.amount, p.title, p.price, p.description, p.category_id FROM order_product AS op
            INNER JOIN products AS p ON op.product_id = p.id
            WHERE order_id =".$this->id;
        $statement = $this->db->query($sql);
        $products = [];
        foreach ($statement->fetchAll() as $productArr){
            $product = [
                'amount' => $productArr['amount'],
                'obj' => new Product($productArr['title'], $productArr['price'], $productArr['description'], $productArr['category_id'])
            ];
            $product['obj']->setId($productArr['id']);
            $products[] = $product;
        }
        return $products;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }




}