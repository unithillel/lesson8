<?php


class DBEntity
{
    protected $db;

    public function __construct()
    {
        $host = 'localhost';
        $dbUser = 'root';
        $dbPassword = '1234';
        $dbName = 'shop';
        try{
            $this->db = new PDO('mysql:host=' . $host . ';dbname=' . $dbName, $dbUser, $dbPassword);
            //Get Exceptions on query errors
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            // SET UTF
            $this->db->exec('SET NAMES "utf8"');
        }catch(Exception $exception){
            echo "Error connecting to db! " . $exception->getCode() . ' message: ' . $exception->getMessage();
            die('Wasn\'t able to connect to db!');
        }
    }


}