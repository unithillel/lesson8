<?php
require_once 'DBEntity.php';

class Category extends DBEntity
{
    protected $id = 0;
    protected $title = 0;

    /**
     * Category constructor.
     * @param int $title
     */
    public function __construct($title)
    {
        parent::__construct();
        $this->title = $title;
    }

    public function store(PDO $pdo)
    {
        try {
            $sql = "INSERT INTO categories SET title = :title ";
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':title', $this->getTitle());
            $statement->execute();
        } catch (Exception $exception) {
            die('Unable to save category' . $exception->getMessage());
        }

    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    static public function all(PDO $pdo){
        try {
            $sql = "SELECT * FROM categories";
            $statement = $pdo->query($sql);
            $categoriesArray = $statement->fetchAll();
            $categoryObj = [];
            foreach ($categoriesArray as $categoryArr){
                $category = new self($categoryArr['title']);
                $category ->setId($categoryArr['id']);
                $categoryObj[] = $category;
            }
            return $categoryObj;

        }catch (Exception $e){

        }
    }

    public function getProducts(){
        $sql = "SELECT * from products WHERE category_id =".$this->id;
        $statement = $this->db->query($sql);
        $productsArr = $statement->fetchAll();
        $productsObjs = [];
        foreach ($productsArr as $product){
            $productObj = new Product($product['title'], $product['price'], $product['description'], $product['category_id']);
            $productObj->setId($product['id']);
            $productsObjs[] = $productObj;
        }
        return $productsObjs;
    }

    static public function getById($id, $pdo){
        try {
            $sql = "SELECT * FROM categories WHERE id=:id";
            $statement = $pdo->prepare($sql);
            $statement->bindValue(':id', $id);
            $statement->execute();
            $categoryArr = $statement->fetchAll();
        }catch (Exception $exception){
            die('Error getting Category'.$exception->getCode());
        }
        $categoryObj = new self($categoryArr[0]['title']);
        $categoryObj->setId($categoryArr[0]['id']);
        return $categoryObj;
    }


}